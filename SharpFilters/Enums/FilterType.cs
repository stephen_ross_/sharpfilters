﻿// Copyright © Stephen Ross 2016

namespace SharpFilters.Enums
{
    public enum FilterType
    {
        Lowpass,

        Highpass
    }
}